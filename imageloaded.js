(function ($) {
    "use strict";

    var ImageLoaded = function () {
        this.container = null;
        this.callback = null;
        this.images = [];

        this.allImagesCount = 0;
        this.loadedImagesCount = 0;
        this.isRunning = false;
    };

    var imgOnLoadListener = function () {
        if (this.allImagesCount === 0) {
            this.isRunning = false;
            return;
        }

        ++this.loadedImagesCount;

        if (this.loadedImagesCount > 0 && this.loadedImagesCount === this.allImagesCount) {
            typeof this.callback === 'function' && this.callback.call(this);
            this.isRunning = false;
        }
    };

    ImageLoaded.prototype.run = function () {
        var me = this;

        if (this.isRunning)
            return;

        this.isRunning = true;

        var srcArr = [];
        this.container.find('img').each(function () {
            var img = new Image();
            $(img).on('load', function () {
                imgOnLoadListener.call(me);
            });

            me.images.push(img);
            srcArr.push($(this).attr('src'));
        });

        this.allImagesCount = this.images.length;

        var i = 0, len = this.allImagesCount, image, src;
        for (; i < len; ++i) {
            image = this.images[i];
            src = srcArr[i];
            image.src = src;
        }
    };

    $.fn.imagesLoaded = function (callback) {
        var il = new ImageLoaded();
        il.container = this;
        il.callback = callback;

        il.run();
    };
})(jQuery);