(function ($) {
    "use strict";

    var innerContainerCss = {
        'font-size': 0,
        '-webkit-text-size-adjust': 'none',
        'position': 'absolute'
    };

    var itemCss = {
        'display': 'inline-table'
    };

    var realWith = function (elem) {
        var rect = elem.getBoundingClientRect();
        return rect.right - rect.left;
    };

    var Gallery = function () {
        this.options = null;

        this.container = null;
        this.innerContainer = null;
        this.items = null;

        this.itemWidth = 0;
        this.itemsCount = 0;

        this.currentIndex = 0;
    };

    Gallery.prototype._init = function () {
        var me = this;

        this.container.html('<div data-role="innerContainer">' + this.container.html() + '</div>');
        this.innerContainer = this.container.find('[data-role="innerContainer"]');
        this.items = this.innerContainer.find('[data-role="item"]');

        this.itemsCount = this.items.length;
        this.innerContainer.css(innerContainerCss);

        var innerContainerWidth = 0, itemWidth;
        this.items.each(function () {
            $(this).css(itemCss);

            itemWidth = realWith(this);
            innerContainerWidth += itemWidth;
            me.itemWidth = itemWidth;
        });

        this.innerContainer.css('width', innerContainerWidth);
    };

    Gallery.prototype.init = function (options) {
        var me = this;

        this.container.css(this.options.containerCss);
        this.container.imagesLoaded(function () {
            me._init(options);
            me._triggerEvent('afterInit');
        });
    };

    Gallery.prototype._triggerEvent = function (eventName, args) {
        var listeners = this.options.listeners, listener = listeners[eventName];
        if (listener) {
            if (typeof  listener === 'function') {
                listener.call(this);
            } else if (listener.length) {
                var i = 0, len = listener.length, l;
                for (; i < len; ++i) {
                    l = listener[i];
                    if (typeof l === 'function')
                        l.apply(this, args);
                }
            }
        }
    };

    Gallery.prototype.to = function (index) {
        index = parseInt(index);
        index = isNaN(index) ? 0 : index;

        if (!this.options.useLoop) {
            index = index < 0 ? 0 : index;
            index = index > this.itemsCount - 1 ? this.itemsCount - 1 : index;
        } else {
            index = index < 0 ? this.itemsCount - 1 : index;
            index = index > this.itemsCount - 1 ? 0 : index;
        }

        var left = -1 * index * this.itemWidth,
            me = this;

        this.innerContainer.clearQueue().stop(true, true);

        this.innerContainer.animate({
            'left': left + 'px'
        }, this.options.animateDuration, function () {
            me.currentIndex = index;
            me._triggerEvent('itemAnimateAfter', [me.currentIndex]);
        });
    };

    Gallery.prototype.next = function () {
        var index = this.currentIndex + 1;
        this.to(index);
    };

    Gallery.prototype.prev = function () {
        var index = this.currentIndex - 1;
        this.to(index);
    };

    $.fn.gallery = function (options) {
        options = $.extend(true, {}, $.fn.gallery.defaultOptions, options);

        var gallery = new Gallery();
        gallery.container = this;
        gallery.options = options;
        gallery.init(options);

        return gallery;
    };

    $.fn.gallery.defaultOptions = {
        useLoop: true,
        animateDuration: 500,
        containerCss: {
            'overflow': 'hidden',
            'position': 'relative'
        },
        listeners: {
            'afterInit': [],
            'itemAnimateAfter': []
        }
    };
})(jQuery);